import re

class Course():
    def __init__(self, name, ctype, sp1, sp2, url):
        self.name = name
        self.ctype = ctype
        self.sp1 = sp1
        self.sp2 = sp2
        self.url = url

    def total_sp(self):
        return self.sp1 + self.sp2
    
    def to_string(self):
        text = "{0:65}".format(self.name)
        text_type = ""
        if bool(re.match("..", self.ctype)):
            text_type += "biennial: "
            if bool(re.match(".H", self.ctype)):
                text_type += "current"
            else:
                text_type += "next"
        elif self.ctype == "S":
            text_type = "selection"
        else:
            text_type = "normal"
        text += " │ {0:17}".format(text_type)

        if self.sp1 > 0:
            text += " │ sem 0"
        if self.sp2 > 0:
            text += " │ sem 1"
        text += f" │ {self.total_sp()}sp"

        return text

class Profile():
    def __init__(self, name):
        self.name = name
        self.courses = []

    def add_course(self, course):
        self.courses.append(course)
    
    def to_string(self):
        text = f"Profile '{self.name}':\n"
        for course_index, course in enumerate(self.courses):
            text += f"{course_index}. {course.to_string()}\n"
        return text

class Trajectory():
    def __init__(self, years=2, semesters=2):
        self.trajectory = [[[] for semester in range(semesters)] for year in range(years)]

    def get_courses(self):
        courses = []
        for year in self.trajectory:
            for semester in year:
                for course in semester:
                    courses.append(course)
        return courses
                

    def add_course(self, course, year):
        if course.name not in [c.name for c in self.get_courses()]:
            if course.sp1 > 0:
                self.trajectory[year][0].append(course)
            if course.sp2 > 0:
                self.trajectory[year][1].append(course)

    def delete_course(self, course):
        found = False
        for year_index, year in enumerate(self.trajectory):
            for semester_index, semester in enumerate(year):
                if course in semester:
                    found = True
                    break
            if found:
                break
        if found:
            self.trajectory[year_index][semester_index].remove(course)
        else:
            print("Course to delete was not found!")

    def print(self):
        print("Current trajectory:")
        total_sp = 0
        for year_index, year in enumerate(self.trajectory):
            print( " ┌────────┐")
            print(f"┌┤ Year {year_index} ├──────────────────────────────┐")
            print( "│└────────┘")
            year_sp = 0
            for semester_index, semester in enumerate(year):
                print(f"│  ┌─────────────────────────────────────")
                print(f"├──┤ Semester {semester_index}")
                print(f"│  ├─────────────────────────────────────")
                sem_sp = 0
                for course in semester:
                    print(f"│  │  {course.name}, {course.total_sp()}")
                    sem_sp += course.total_sp()
                print( "│  │  ")
                print(f"│  │  Total: {sem_sp}")
                print(f"│  └─────────────────────────────────────")
                year_sp += sem_sp
            print(f"│ Year {year_index} total: {year_sp} ({60 - year_sp} left)")
            print("└────────────────────────────────────────┘")
            total_sp += year_sp
        print(f"Trajectory total: {total_sp}")


