import os
import requests
from urllib.parse import urljoin
from sys import argv, exit
from bs4 import BeautifulSoup
import pickle
import models
import re

SAVE_FILE = ".mastertrajectory.sav"
base_url = "https://uhintra03.uhasselt.be/studiegidswww/opleiding.aspx?t=01"
payload = {'__EVENTTARGET': 'beschridDDL$ctl00',
'beschridDDL$ctl00': '151',
'beschridAcjaar$ctl00': '2020',
'__VIEWSTATE': '/wEPDwUKMTA0NDQzNDU5Mg9kFgICAw9kFggCCQ9kFgJmDxAPFgIeDEF1dG9Qb3N0QmFja2cWAh4Fc3R5bGUFaSBmb250LWZhbWlseTogVmVyZGFuYSxBcmlhbCxIZWx2ZXRpY2EsU2Fucy1zZXJpZjsgZm9udC1zaXplOiAxMXB4OyBoZWlnaHQ6MThweDsgd2lkdGg6NjAwcHg7IGZsb2F0OmNlbnRlchAVCwsyMDIwIC0gMjAyMQsyMDE5IC0gMjAyMAsyMDE4IC0gMjAxOQsyMDE3IC0gMjAxOAsyMDE2IC0gMjAxNwsyMDE1IC0gMjAxNgsyMDE0IC0gMjAxNQsyMDEzIC0gMjAxNAsyMDEyIC0gMjAxMwsyMDExIC0gMjAxMgsyMDEwIC0gMjAxMRULBDIwMjAEMjAxOQQyMDE4BDIwMTcEMjAxNgQyMDE1BDIwMTQEMjAxMwQyMDEyBDIwMTEEMjAxMBQrAwtnZ2dnZ2dnZ2dnZxYBZmQCCw9kFgJmDxAPZBYCHwEFaSBmb250LWZhbWlseTogVmVyZGFuYSxBcmlhbCxIZWx2ZXRpY2EsU2Fucy1zZXJpZjsgZm9udC1zaXplOiAxMXB4OyBoZWlnaHQ6MThweDsgd2lkdGg6NjAwcHg7IGZsb2F0OmNlbnRlchAVNCJIaWVyIGthbiBqZSBkZSBvcGxlaWRpbmcga2llemVuLi4uGWJhY2hlbG9yIGhhbmRlbHNpbmdlbmlldXIyYmFjaGVsb3IgaGFuZGVsc2luZ2VuaWV1ciBpbiBkZSBiZWxlaWRzaW5mb3JtYXRpY2EbYmFjaGVsb3IgaW4gZGUgYXJjaGl0ZWN0dXVyF2JhY2hlbG9yIGluIGRlIGJpb2xvZ2llKGJhY2hlbG9yIGluIGRlIGJpb21lZGlzY2hlIHdldGVuc2NoYXBwZW4VYmFjaGVsb3IgaW4gZGUgY2hlbWllFWJhY2hlbG9yIGluIGRlIGZ5c2ljYRpiYWNoZWxvciBpbiBkZSBnZW5lZXNrdW5kZSNiYWNoZWxvciBpbiBkZSBoYW5kZWxzd2V0ZW5zY2hhcHBlbiliYWNoZWxvciBpbiBkZSBpbmR1c3RyacOrbGUgd2V0ZW5zY2hhcHBlbhpiYWNoZWxvciBpbiBkZSBpbmZvcm1hdGljYSRiYWNoZWxvciBpbiBkZSBpbnRlcmlldXJhcmNoaXRlY3R1dXInYmFjaGVsb3IgaW4gZGUgbW9iaWxpdGVpdHN3ZXRlbnNjaGFwcGVuFmJhY2hlbG9yIGluIGRlIHJlY2h0ZW48YmFjaGVsb3IgaW4gZGUgcmV2YWxpZGF0aWV3ZXRlbnNjaGFwcGVuIGVuIGRlIGtpbmVzaXRoZXJhcGllM2JhY2hlbG9yIGluIGRlIHRvZWdlcGFzdGUgZWNvbm9taXNjaGUgd2V0ZW5zY2hhcHBlbhdiYWNoZWxvciBpbiBkZSB3aXNrdW5kZSBFZHVjYXRpZXZlIG1hc3RlciBpbiBkZSBlY29ub21pZTBFZHVjYXRpZXZlIG1hc3RlciBpbiBkZSBnZXpvbmRoZWlkc3dldGVuc2NoYXBwZW4sRWR1Y2F0aWV2ZSBtYXN0ZXIgaW4gZGUgb250d2VycHdldGVuc2NoYXBwZW40RWR1Y2F0aWV2ZSBtYXN0ZXIgaW4gZGUgd2V0ZW5zY2hhcHBlbiBlbiB0ZWNobm9sb2dpZQZsZXJhYXIXbWFzdGVyIGhhbmRlbHNpbmdlbmlldXIwbWFzdGVyIGhhbmRlbHNpbmdlbmlldXIgaW4gZGUgYmVsZWlkc2luZm9ybWF0aWNhGW1hc3RlciBpbiBkZSBhcmNoaXRlY3R1dXImbWFzdGVyIGluIGRlIGJpb21lZGlzY2hlIHdldGVuc2NoYXBwZW4hbWFzdGVyIGluIGRlIGhhbmRlbHN3ZXRlbnNjaGFwcGVuMm1hc3RlciBpbiBkZSBpbmR1c3RyacOrbGUgd2V0ZW5zY2hhcHBlbjogYmlvY2hlbWllMm1hc3RlciBpbiBkZSBpbmR1c3RyacOrbGUgd2V0ZW5zY2hhcHBlbjogYm91d2t1bmRlL21hc3RlciBpbiBkZSBpbmR1c3RyacOrbGUgd2V0ZW5zY2hhcHBlbjogY2hlbWllOW1hc3RlciBpbiBkZSBpbmR1c3RyacOrbGUgd2V0ZW5zY2hhcHBlbjogZWxla3Ryb21lY2hhbmljYThtYXN0ZXIgaW4gZGUgaW5kdXN0cmnDq2xlIHdldGVuc2NoYXBwZW46IGVsZWt0cm9uaWNhLUlDVDBtYXN0ZXIgaW4gZGUgaW5kdXN0cmnDq2xlIHdldGVuc2NoYXBwZW46IGVuZXJnaWU+bWFzdGVyIGluIGRlIGluZHVzdHJpw6tsZSB3ZXRlbnNjaGFwcGVuOiBudWNsZWFpcmUgdGVjaG5vbG9naWU/bWFzdGVyIGluIGRlIGluZHVzdHJpw6tsZSB3ZXRlbnNjaGFwcGVuOiB2ZXJwYWtraW5nc3RlY2hub2xvZ2llGG1hc3RlciBpbiBkZSBpbmZvcm1hdGljYSJtYXN0ZXIgaW4gZGUgaW50ZXJpZXVyYXJjaGl0ZWN0dXVyJW1hc3RlciBpbiBkZSBtb2JpbGl0ZWl0c3dldGVuc2NoYXBwZW4UbWFzdGVyIGluIGRlIHJlY2h0ZW46bWFzdGVyIGluIGRlIHJldmFsaWRhdGlld2V0ZW5zY2hhcHBlbiBlbiBkZSBraW5lc2l0aGVyYXBpZTFtYXN0ZXIgaW4gZGUgdG9lZ2VwYXN0ZSBlY29ub21pc2NoZSB3ZXRlbnNjaGFwcGVuHU1hc3RlciBvZiBCaW9tZWRpY2FsIFNjaWVuY2VzH01hc3RlciBvZiBJbnRlcmlvciBBcmNoaXRlY3R1cmUUTWFzdGVyIG9mIE1hbmFnZW1lbnQlTWFzdGVyIG9mIFN0YXRpc3RpY3MgYW5kIERhdGEgU2NpZW5jZSFNYXN0ZXIgb2YgVHJhbnNwb3J0YXRpb24gU2NpZW5jZXMaUG9zdGdyYWR1YWF0IEJlZHJpamZza3VuZGUxUG9zdGdyYWR1YWF0IEJpb2dlYmFzZWVyZGUgZW4gY2lyY3VsYWlyZSBlY29ub21pZTJQb3N0Z3JhZHVhYXQgSW5ub3ZlcmVuZCBPbmRlcm5lbWVuIHZvb3IgaW5nZW5pZXVyczJQb3N0Z3JhZHVhYXQgUmVsYXRpZS0gZW4gQ29tbXVuaWNhdGlld2V0ZW5zY2hhcHBlbkdQb3N0Z3JhZHVhdGUgY2VydGlmaWNhdGUgSW5ub3ZhdGlvbiBhbmQgRW50cmVwcmVuZXVyc2hpcCBpbiBFbmdpbmVlcmluZxU0AAM2NDgDNjQ5A0gwMQMwNTUDMjI1AzA0NQMwMjUDMDY1AzU5NwNIMjADMTM1A0gwNQM3NTADMjk0A0g3MAM2MjUDMDE1A0U2MANFNjIDRTYxA0UyOAM3MzYDNjU2AzY1NQNIMDkDMjQ3AzYwNQNINjgDSDk1A0g2MgNINjMDSDY0A0g1MANIOTADSDk0AzE1MQNIMTIDNzQ5AzcxMQNINzQDNjI4AzI3OQNIMTUDODMxAzM0OQM3NjgDUDcwA1A1MQNQMzEDUDI3A1A0NxQrAzRnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnFgFmZAINDw8WAh4EVGV4dGVkZAIPD2QWAmYPDxYCHwIFnAI8ZGl2IGFsaWduPSJjZW50ZXIiPjxhIGhyZWY9ImphdmFzY3JpcHQ6c3dlZXB0b2dnbGUoJ2V4cGFuZCcpIj5Ub29uIGFsbGUgaXRlbXMgPC9hPiB8IDxhIGhyZWY9ImphdmFzY3JpcHQ6c3dlZXB0b2dnbGUoJ2NvbnRyYWN0JykiPlZlcmJlcmcgYWxsZSBpdGVtcyA8L2E+IDwvZGl2Pjx0YWJsZSBpZD0iVGFibGUyaCIgY2VsbFNwYWNpbmc9IjAiIGNlbGxQYWRkaW5nPSIwIiB3aWR0aD0iMTAwJSIgYm9yZGVyPSIwIiBhbGlnbj0iY2VudGVyIj48dHI+PHRkPjwvdGQ+PC90cj48L3RhYmxlPjwvZGl2PmRkZLJE9bwOHexyqppNxtARXhvrEHlQ1/gZ/EFUIyF/pn5I'}
headers = {
  'Cookie': 'f5_cspm=1234; ASP.NET_SessionId=1h2i1iopgqj3htwx0crxno3l; TS01bf64b5=0129aa0f8f989c41977c8b8101692bfa17e754e1d1b759adb441eb12a4fa090268e406ed99e032fd30fbca04aea2cee7acd326d6471f0e571ab94c2ff766e87af180bf4de2a42d8afe348d226f004c21cde9a47a9f'
}

def print_help():
    global command_map
    print("Known commands:")
    for command in command_map:
        print(" "+command+": "+command_map[command][0])

def get_description_from_url(url):
    response = requests.request("POST", url, headers=headers)
    soup= BeautifulSoup(response.text, "html.parser")

    base_elem = soup.find("table", {"id":"VkSgteam1_ctl03_gvContent"}).find("tr").find("td")
    text = ' '.join(base_elem.find_all(text=True, recursive=True)).rstrip()

    return text

def select_profile():
    global profiles

    print()
    for index, profile in enumerate(profiles):
        print("{0:2}. {1}".format(index, profile.name))
    print()
    inp = input(f"Please select a course profile from the list [0-{len(profiles)-1}, c=cancel]: ")

    try:
        profile_index = int(inp)
        return profiles[profile_index]
    except:
        return None

def print_profile_courses(profile):
    print(f"'{profile.name}' courses:")
    for index, course in enumerate(profile.courses):
        print("│{0:2}. {1}│".format(index, course.to_string()))
        # print("└───"+"─"*len(course.to_string())+"─┘")
    print()

def select_course():
    done = False
    while not done:
        profile = select_profile()
        if profile is not None:
            print_profile_courses(profile)
            inp = input(f"Please select a course [0-{len(profile.courses)-1}, b=back]: ")
            done = True
            if inp == "b" or inp == "B" or inp == "back":
                done = False
        else:
            done = True
    try:
        course_index = int(inp)
        return profile.courses[course_index]
    except:
        return None

def get_information():
    course = select_course()
    
    if course is not None:
        print()
        print(30*'-')
        print(get_description_from_url(course.url)[1:])
        print(30*'-')

def explore():
    profile = select_profile()
    if profile is not None:
        print_profile_courses(profile)

def add_course():
    global trajectory
    course = select_course()
    if course is not None:
        year = -1
        if bool(re.match(".H", course.ctype)):
            year = 0
        elif bool(re.match(".V", course.ctype)):
            year = 1
        else:
            year = int(input(f"Please select year [0-{len(trajectory.trajectory)-1}] for course '{course.name}': "))

        trajectory.add_course(course, year)
        print("Course was successfully added to trajectory.")

def add_profile():
    global trajectory
    profile = select_profile()

    if profile is not None:
        print(f"Adding '{profile.name}' courses...")
        for course in profile.courses:
            year = -1
            if bool(re.match(".H", course.ctype)):
                year = 0
            elif bool(re.match(".V", course.ctype)):
                year = 1
            else:
                year = int(input(f"Please select year [0-{len(trajectory.trajectory)-1}] for course '{course.name}': "))
            trajectory.add_course(course, year)
        print("Profile was successfully added to trajectory.")

def delete_course():
    global trajectory, profiles
    courses = trajectory.get_courses()
    try:
        for index, course in enumerate(courses):
            print(f"{index}. {course.to_string()}")
        print()
        delete_index = int(input(f"Please select a course to delete [0-{len(courses)-1}, c=cancel]: "))
        trajectory.delete_course(courses[delete_index])
        print("Course was deleted")
    except:
        print("Cancelled.")

def save_trajectory():
    global trajectory
    print("Saving trajectory...")
    with open(SAVE_FILE, "wb") as f:
        pickle.dump(trajectory, f)
        
def load_trajectory():
    global trajectory
    print("Loading saved trajectory...")
    with open(SAVE_FILE, "rb") as f:
        trajectory = pickle.load(f)

def quit_app():
    print("Exiting...")
    exit(0)

def print_trajectory():
    global trajectory
    trajectory.print()

def print_courses():
    global trajectory
    for course in trajectory.get_courses():
        print(f"{course.to_string()}")
            
command_map = { 
                '?': ["print this message", print_help],
                'p': ["print current trajectory", print_trajectory],
                'pl': ["print flat list of courses in trajectory", print_courses],
                'a': ["add course to trajectory", add_course],
                'ap': ["add all courses in profile to trajectory", add_profile],
                'd': ["delete course from trajectory", delete_course],
                'i': ["get more information about specified course", get_information],
                'e': ["explore course profile contents", explore],
                's': ["save current trajectory", save_trajectory],
                'l': ["load previous trajectory save", load_trajectory],
                'q': ["quit", quit_app]
                }

def load_data():
    global trajectory, profiles
    profiles = []
    trajectory = models.Trajectory()

    print("Loading data...")
    response = requests.request("POST", base_url, headers=headers, data = payload)
    soup= BeautifulSoup(response.text, "html.parser")
    base_div = soup.find("div", {"id":"sc52"})
    base_table = base_div.find("table")
    sub_tables = base_table.find_all("table")
    for table_index, table in enumerate(sub_tables[:-1]):
        table_name = table.find("tr").find("td").find(text=True, recursive=False).rstrip()
        profile = models.Profile(table_name)
        for course_tr in table.find_all("tr")[1:]:
            tds = course_tr.find_all("td")
            url = tds[0].find("a")["href"]
            name = tds[0].find("a").find(text=True, recursive=True).rstrip()
            ctype = tds[2].find(text=True, recursive=True).rstrip()
            sp1 = 0
            sp2 = 0
            td_sp1 = tds[6].find(text=True, recursive=True)
            if td_sp1 is not None:
                sp1 = int(td_sp1.rstrip())
            td_sp2 = tds[8].find(text=True, recursive=True)
            if td_sp2 is not None:
                sp2 = int(td_sp2.rstrip())
            profile.add_course(models.Course(name,ctype,sp1,sp2,url))
        profiles.append(profile)

if __name__ == "__main__":
    load_data()

    while True:
        print()
        command = input("Enter next command (? for help): ")

        if command in command_map:
            command_map[command][1]()
        else:
            print("Unknown command.")
